import pickle
import matplotlib.pyplot as plt


def analyze():

    last_run_acc = pickle.load(open("pickles/last_run_acc.p", "rb"))
    last_run_val_acc = pickle.load(open("pickles/last_run_val_acc.p", "rb"))
    last_run_loss = pickle.load(open("pickles/last_run_loss.p", "rb"))
    last_run_val_loss = pickle.load(open("pickles/last_run_val_loss.p", "rb"))

    print(last_run_acc)

    # summarize history for accuracy
    plt.figure(figsize=(15, 5))
    plt.subplot(1, 2, 1)
    plt.plot(last_run_acc)
    plt.plot(last_run_val_acc)
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'valid'], loc='upper left')

    # summarize history for loss
    plt.subplot(1, 2, 2)
    plt.plot(last_run_loss)
    plt.plot(last_run_val_loss)
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'valid'], loc='upper left')
    plt.show()
    plt.draw()


if __name__ == "__main__":
    analyze()
