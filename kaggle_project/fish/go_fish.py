from __future__ import division, print_function
import glob
from keras.callbacks import EarlyStopping
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D, UpSampling2D, Cropping2D, AveragePooling2D
from keras.layers.core import Flatten, Dense, Dropout, Lambda
from keras.layers.normalization import BatchNormalization
from keras.models import Sequential
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import pickle
import matplotlib.pyplot as plt

# NOTE: PIL must be installed (pip install Pillow)
# Use dev installation of Theano
#     git clone git://github.com/Theano/Theano.git
#     cd Theano
#     pip install -e .

# ~/.keras/keras.json
# {
#     "image_dim_ordering": "th",
#     "epsilon": 1e-07,
#     "floatx": "float32",
#     "backend": "theano"
# }

# Setting up Directories
home = os.getcwd()
os.system('rm -r ../train')
os.mkdir('../train')
os.system('cp -r ../train_orig/* ../train')
os.system('rm -r ../valid')
os.mkdir('../valid')
os.chdir('../train')

# Create empty dirs matching /train/* in /valid/*
for fish_class in glob.glob('*'):
    os.mkdir('../valid/' + fish_class)

# Create random ordered image file name array
rnd_ord = np.random.permutation(glob.glob('*/*.jpg'))

# Create 500 random files in /valid/* using first 500 images files from rnd_ord
for i in range(500):
    os.rename(rnd_ord[i], '../valid/' + rnd_ord[i])

# Create dimensional arrays -> [ [[x]], [[y]], [[z]] ]
vgg_mean = np.array([123.68, 116.779, 103.939], dtype=np.float32).reshape((3, 1, 1))


# Preprocess VGG array
def vgg_preprocess(x):
    x = x - vgg_mean
    return x[:, ::-1]


# Add Convolution Neural Network layers   ### weights_path=home + '/vgg16_weights.h5'
def vgg_16(size=(224, 224), weights_path=None): #weights_path='../pickles/vgg16_aug_lessdrop.pkl'):
    model = Sequential()
    model.add(Lambda(vgg_preprocess, input_shape=(3,) + size, output_shape=(3,) + size))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(64, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(64, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), dim_ordering="th"))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(128, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(128, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), dim_ordering="th"))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(UpSampling2D(size=(2, 2)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), dim_ordering="th"))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(Cropping2D(cropping= ((1, 1),(1,1)) ))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(AveragePooling2D((2, 2), strides=(2, 2), dim_ordering="th"))

    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1, 1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2, 2), strides=(2, 2), dim_ordering="tf"))

    model.add(Flatten())
    model.add(Dense(4096, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.5))
    model.add(Dense(4096, activation='relu'))
    model.add(BatchNormalization())
    model.add(Dropout(0.5))
    model.add(Dense(1000, activation='softmax'))

    if weights_path:
        model.load_weights(weights_path)
        # TODO change load_weights to load_weights_from_hdf5_group with new directory

    return model


size = (224, 224)
model = vgg_16(size=size)
model.pop()
model.pop()
model.pop()
model.pop()
model.pop()

for layer in model.layers:
    layer.trainable = False

model.add(Dropout(0.2))
model.add(Dense(512, activation='relu'))
model.add(BatchNormalization())
model.add(Dropout(0.3))
model.add(Dense(8, activation='softmax'))

nb_train_samples = len(glob.glob('*/*.jpg'))
nb_validation_samples = len(glob.glob('../valid/*/*.jpg'))
nb_epoch = 50 # number of rounds training
batch_size = 29 # batch number of 3700ish images for training
nb_test_samples = 13153 # Total number of files in ../test/*

classes = ['ALB', 'BET', 'DOL', 'LAG', 'NoF', 'OTHER', 'SHARK', 'YFT']

train_data_gen = ImageDataGenerator(
    shear_range=0.2,
    zoom_range=0.1,
    rotation_range=10.,
    width_shift_range=0.2,
    height_shift_range=0.2,
    horizontal_flip=True)

train_generator = train_data_gen.flow_from_directory(
    '../train/',
    target_size=size,
    batch_size=batch_size,
    shuffle=True,
    classes=classes,
    class_mode='categorical')

valid_data_gen = ImageDataGenerator()

validation_generator = valid_data_gen.flow_from_directory(
    '../valid/',
    target_size=size,
    batch_size=batch_size,
    shuffle=True,
    classes=classes,
    class_mode='categorical')

callbacks = [EarlyStopping(monitor='val_loss', patience=1, verbose=1)] # THIS IS WHAT MAKES IT STOP EARLY!!!
model.compile(loss='categorical_crossentropy', optimizer="adadelta", metrics=["accuracy"])

# Fine-tune the model
hist = model.fit_generator(train_generator, samples_per_epoch=nb_train_samples, nb_epoch=nb_epoch,
                           validation_data=validation_generator, nb_val_samples=nb_validation_samples,
                           callbacks=callbacks, verbose=1)

# Save Weights and Training History Graph
os.system('rm -r ../pickles')
os.mkdir('../pickles')
model.save_weights('../pickles/vgg16_aug_lessdrop.pkl')

last_run_acc = "../pickles/last_run_acc.p"
last_run_val_acc = "../pickles/last_run_val_acc.p"
last_run_loss = "../pickles/last_run_loss.p"
last_run_val_loss = "../pickles/last_run_val_loss.p"
pickle.dump(hist.history['acc'], open(last_run_acc, "wb"))
pickle.dump(hist.history['val_acc'], open(last_run_val_acc, "wb"))
pickle.dump(hist.history['loss'], open(last_run_loss, "wb"))
pickle.dump(hist.history['val_loss'], open(last_run_val_loss, "wb"))

#sys.setrecursionlimit(50000)
#last_run = "../pickles/last_run_plot.p"
#pickle.dump(plt, open(last_run, "wb"))

# Test Generator
test_aug = 5 # Number of Tests to run
test_data_gen = ImageDataGenerator(
        shear_range=0.2,
        zoom_range=0.1,
        rotation_range=10.,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

predictions = None

for aug in range(test_aug):
    print('Predictions for Augmented -', aug)
    random_seed = np.random.random_integers(0, 100000)
    test_generator = test_data_gen.flow_from_directory(
            '../test/',
            target_size=size,
            batch_size=batch_size,
            shuffle=False,
            seed=random_seed,
            classes=None,
            class_mode='categorical')

    test_image_list = test_generator.filenames
    if aug == 0:
        predictions = model.predict_generator(test_generator, nb_test_samples)
    else:
        predictions += model.predict_generator(test_generator, nb_test_samples)

predictions /= test_aug


# Clip predictions
pool_counter = 1
c = 0
preds = np.clip(predictions, c, 1-c)

print('Begin to write submission file ..')

os.system('rm -r ../pooled')
os.mkdir('../pooled')
f_submit = open(os.path.join('../pooled',
                             'submit_' + str(pool_counter) + '.csv'), 'w')
f_submit.write('image,ALB,BET,DOL,LAG,NoF,OTHER,SHARK,YFT\n')
for i, image_name in enumerate(test_generator.filenames):
    pred = ['%.6f' % p for p in preds[i, :]]
    if i%500 == 0:
        print(i, '/', 1000)
    f_submit.write('%s,%s\n' % (os.path.basename(image_name), ','.join(pred)))

f_submit.close()
