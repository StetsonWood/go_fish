# README #

### What is this repository for? ###

* GoFish! [Kaggle Competition put on by The Nature Conservancy](https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring)
* Alpha 0.0.0
* MaryLou slurm file included

### How do I get set up? ###

* This is set up to work on MaryLou
* NOTE: You must add the test images and train images from the project before running the code. Download them [here](https://www.kaggle.com/c/the-nature-conservancy-fisheries-monitoring/data).
* Create conda environment named "kagenv" -> conda env create -f environment.yml
* Add batch using SLURM -> sbatch runJob.sh

### Notes ###

* The images (train and test) are not included in this repository. Must be added as follows:
```
root
│   README.md    
│   ...
└───train_orig/
│   └───ALB/
│       │   image0003.txt
│       │   ...
│   └───BET/
│       │   image0107.txt
│       │   ...
│   └───DOL/
│       │   image0165.txt
│       │   ...
│   └───LAG/
│       │   image0091.txt
│       │   ...
│   └───NoF/
│       │   image0008.txt
│       │   ...
│   └───OTHER/
│       │   image0063.txt
│       │   ...
│   └───SHARK/
│       │   image0033.txt
│       │   ...
│   └───YFT/
│       │   image0004.txt
│       │   ...
└───test/
│   └───test_stg1/
│       │   image0005.txt
│       │   ...
│   └───test_stg2/
│       │   image0007.txt
│       │   ...
```
* This is just a functioning version of our code

### Who do I talk to? ###

* Donald Ford : donaldthomasford@gmail.com
* Stetson Wood : stetson.wood@gmail.com